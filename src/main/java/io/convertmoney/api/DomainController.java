package io.convertmoney.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.convertmoney.domain.model.ResponseApi;
import io.convertmoney.domain.model.dto.MoedasDto;
import io.convertmoney.domain.service.ApiBancoBrasilService;

@RestController
@RequestMapping("/domain")
public class DomainController {
	
	@Autowired
	private ApiBancoBrasilService service;
	
	
	@CrossOrigin(origins = "*")
	@GetMapping("/moedas")
	public ResponseEntity<ResponseApi<MoedasDto>> getMoedas() {
		ResponseApi<MoedasDto> response = new ResponseApi<MoedasDto>();
		response.setDataLists(service.getMoedas().getValue());
		return ResponseEntity.ok(response);
	}
	

}
