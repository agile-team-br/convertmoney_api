package io.convertmoney.domain.model.dto;

import java.math.BigDecimal;

public class ConvertDto {
	
	private String dataCotacao;

	private String moedaOrigem;

	private String moedaFinal;

	private BigDecimal valor;

	public String getDataCotacao() {
		return dataCotacao;
	}

	public void setDataCotacao(String dataCotacao) {
		this.dataCotacao = dataCotacao;
	}

	public String getMoedaOrigem() {
		return moedaOrigem;
	}

	public void setMoedaOrigem(String moedaOrigem) {
		this.moedaOrigem = moedaOrigem;
	}

	public String getMoedaFinal() {
		return moedaFinal;
	}

	public void setMoedaFinal(String moedaFinal) {
		this.moedaFinal = moedaFinal;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	

}
