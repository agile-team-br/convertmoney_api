package io.convertmoney.domain.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MoedasDto {
	
	List<MoedaDto> value = new ArrayList<>();

	public List<MoedaDto> getValue() {
		return value;
	}

	public void setValue(List<MoedaDto> value) {
		this.value = value;
	}

}


class MoedaDto {
	
	private String simbolo;
	private String nomeFormatado;
	private String tipoMoeda;
	
	public String getSimbolo() {
		return simbolo;
	}
	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
	public String getNomeFormatado() {
		return nomeFormatado;
	}
	public void setNomeFormatado(String nomeFormatado) {
		this.nomeFormatado = nomeFormatado;
	}
	public String getTipoMoeda() {
		return tipoMoeda;
	}
	public void setTipoMoeda(String tipoMoeda) {
		this.tipoMoeda = tipoMoeda;
	}
	
	
}
