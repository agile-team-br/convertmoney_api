package io.convertmoney.domain.service.impl;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import io.convertmoney.domain.model.dto.MoedasDto;
import io.convertmoney.domain.service.ApiBancoBrasilService;

@Service
public class ApiBancoBrasilServiceImpl implements ApiBancoBrasilService {

	private String url = "";

	private RestTemplate restTemplate;

	private MoedasDto moedas;

	public ApiBancoBrasilServiceImpl(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	@Override
	public MoedasDto getMoedas() {


		String url = "https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/Moedas";

		try {
			moedas = restTemplate.getForObject(url, MoedasDto.class);

		} catch (Exception e) {
			System.out.println("erro>> ");
		}

		return moedas;
	}

}
