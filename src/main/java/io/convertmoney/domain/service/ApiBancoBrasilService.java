package io.convertmoney.domain.service;

import io.convertmoney.domain.model.dto.MoedasDto;

public interface ApiBancoBrasilService {

	
	public MoedasDto getMoedas();
	
}
